import QtQuick 2.7
import cyberViking.Qr 1.0


Page1Form {
    button1.onClicked: {
        //console.log("Button Pressed. Entered text: " + qr_text.text);
        qrText.url = qr_text.text
    }
    sb_scale.onValueChanged: {
        //console.log(sb_scale.value)
        settings.qr_scale = sb_scale.value
    }
}
