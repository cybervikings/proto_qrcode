#include "qrencoder.h"
#include <QImage>
#include <QPainter>
#include<QDebug>

QrEncoder::QrEncoder(QQuickItem *parent)
                    :QQuickPaintedItem(parent)
{
    connect(this,&QrEncoder::urlChanged,this,&QrEncoder::update);
}

void QrEncoder::paint(QPainter *painter)
{
    int levelIndex = 1;
    int versionIndex = 0;
    bool bExtent = true;
    int maskIndex = -1;
    QString encodeString =  this->url.toString();

    if (encodeString.isEmpty())
        return;

    // evaluates if encoding is successful. I left it for future features
    successfulEncoding = encoder.EncodeData( levelIndex, versionIndex, bExtent, maskIndex, encodeString.toUtf8().data() );

    int qrImageSize = encoder.m_nSymbleSize;
    encodeImageSize = qrImageSize + ( QR_MARGIN * 2 );
    QImage encodeImage( encodeImageSize, encodeImageSize, QImage::Format_Mono );
    encodeImage.fill( 1 );

    for ( int i = 0; i < qrImageSize; i++ )
        for ( int j = 0; j < qrImageSize; j++ )
            if ( encoder.m_byModuleData[i][j] )
                encodeImage.setPixel( i + QR_MARGIN, j + QR_MARGIN, 0 );

    painter->scale(8,8);

    painter->drawImage(QRect(0,0,qrImageSize,qrImageSize), encodeImage);

}



QUrl QrEncoder::getUrl() const
{
    return url;
}

void QrEncoder::setUrl(const QUrl &value)
{
    url = value;
    emit urlChanged();
}


void QrEncoder::update()
{
    QQuickPaintedItem::update(QRect());
}
