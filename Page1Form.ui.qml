import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import cyberViking.Qr 1.0

Item {
    property alias button1: button1
    property alias qr_text: qr_text
    property alias qrText: qrText
    property alias sb_scale: sb_scale

    RowLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top

        TextField {
            id: qr_text
            placeholderText: qsTr("Text Field")
        }

        Button {
            id: button1
            text: qsTr("Press Me")
        }

        SpinBox {
            id: sb_scale
            value: settings.qr_scale
        }
    }

    QrEncoder {
        id: qrText
        x: 167
        y: 94
        width: 5000
        height: 5000
    }

}
