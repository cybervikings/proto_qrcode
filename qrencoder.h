#ifndef QRENCODER_H
#define QRENCODER_H

#include <QQuickPaintedItem>
#include <qrcodegenerator.h>


class QPainter;

class QrEncoder : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ getUrl WRITE setUrl NOTIFY urlChanged)

public:
    QrEncoder(QQuickItem *parent = 0);
    void paint(QPainter *painter); 

    QUrl getUrl() const;
    void setUrl(const QUrl &value);

signals:
    void urlChanged();

private:
    QUrl url;
    CQR_Encode encoder;
    bool successfulEncoding;
    int encodeImageSize;

private slots:
    void update();

};

#endif // QRENCODER_H
